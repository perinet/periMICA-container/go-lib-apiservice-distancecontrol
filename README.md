# API Service DistanceControl

The library module apiservice/distancecontrol implements the RESTful API for the
application part of the DistanceControl setup for Perinets Starter Kit Plus.
It`s a tiny example to demonstrate the recommended structure for container
development for the periMICA.


# Dual License

This software is by default licensed via the GNU Affero General Public License version 3. However it is also available with a commercial license on request (https://perinet.io/contact).