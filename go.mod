module gitlab.com/perinet/periMICA-container/apiservice/distancecontrol

go 1.18

require (
	gitlab.com/perinet/generic/apiservice/mqttclient v0.0.0-20240219163049-da007e421195
	gitlab.com/perinet/generic/app/distancecontrol v0.0.0-20240219163746-dc9aac8af265
	gitlab.com/perinet/generic/lib/httpserver v0.0.0-20230721113448-51dceb287282
	gitlab.com/perinet/generic/lib/utils v0.0.0-20230628130910-2f4300736d64
	gitlab.com/perinet/periMICA-container/apiservice/node v0.0.0-20230404130054-c554bcd34ded
	gotest.tools/v3 v3.5.1
)

require (
	github.com/alexandrevicenzi/go-sse v1.6.0 // indirect
	github.com/eclipse/paho.mqtt.golang v1.4.2 // indirect
	github.com/felixge/httpsnoop v1.0.3 // indirect
	github.com/godbus/dbus/v5 v5.1.0 // indirect
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/gorilla/mux v1.8.1 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/grantae/certinfo v0.0.0-20170412194111-59d56a35515b // indirect
	github.com/holoplot/go-avahi v1.0.1 // indirect
	github.com/tidwall/gjson v1.14.4 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	gitlab.com/perinet/generic/apiservice/dnssd v0.0.0-20230628125338-c9bed005204b // indirect
	gitlab.com/perinet/periMICA-container/apiservice/security v0.0.0-20240116114005-8c49762a48b7 // indirect
	golang.org/x/exp v0.0.0-20230626212559-97b1e661b5df // indirect
	golang.org/x/net v0.1.0 // indirect
	golang.org/x/sync v0.0.0-20220929204114-8fcdb60fdcc0 // indirect
)
