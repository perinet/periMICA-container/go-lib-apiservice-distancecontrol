/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package distancecontrol

import (
	"encoding/json"
	"io"
	"log"
	"net/http"

	"gitlab.com/perinet/generic/apiservice/mqttclient"
	"gitlab.com/perinet/generic/lib/httpserver"
	"gitlab.com/perinet/generic/lib/httpserver/rbac"

	"gitlab.com/perinet/generic/lib/utils/filestorage"
	"gitlab.com/perinet/generic/lib/utils/webhelper"

	"gitlab.com/perinet/periMICA-container/apiservice/node"

	app_distancecontrol "gitlab.com/perinet/generic/app/distancecontrol"
)

const (
	API_VERSION = "20"
)

var (
	configurationFilePath = "/var/lib/distanceControl/config.json"
	distanceController    *app_distancecontrol.DistanceController
	contactor_publisher   mqttclient.MQTTClientPublisherEndpoint
	distances_subscriber  mqttclient.MQTTClientSubscriberEndpoint
)

// Struct definition of DistanceControlInfo used for this service
type DistanceControlInfo struct {
	ApiVersion string                `json:"api_version"`
	Config     DistanceControlConfig `json:"config"`
}

// Struct definition of DistanceControlConfig
type DistanceControlConfig struct {
	ContactorElementName string `json:"contactor_element_name"`
	DistanceElementName  string `json:"distance_element_name"`
	LimitValue           int    `json:"limit_value"`
}

// define the initial distanceControlConfig variable
var distanceControlConfig DistanceControlConfig = DistanceControlConfig{
	ContactorElementName: "contactor_switch",
	DistanceElementName:  "distance",
	LimitValue:           100,
}

// service implementation URL's definition
func PathsGet() []httpserver.Endpoint {
	return []httpserver.Endpoint{
		{Url: "/distancecontrol", Method: httpserver.GET, Role: rbac.USER, Call: DistanceControl_Info_Get},
		{Url: "/distancecontrol/config", Method: httpserver.GET, Role: rbac.USER, Call: DistanceControl_Config_Get},
		{Url: "/distancecontrol/config", Method: httpserver.PATCH, Role: rbac.ADMIN, Call: DistanceControl_Config_Set},
	}
}

// init function which load the config from the file
func init() {
	distanceControlConfig.loadFromFile()

	var nodeInfo node.NodeInfo
	var err error
	data := webhelper.InternalGet(node.NodeInfoGet)
	err = json.Unmarshal(data, &nodeInfo)
	if err != nil {
		log.Println("Failed to fetch NodeInfo: ", err.Error())
	}

	contactor_publisher = mqttclient.NewPublisherEndpoint(nodeInfo.Config.ApplicationName + "/" + distanceControlConfig.ContactorElementName)
	distances_subscriber = mqttclient.NewSubscriberEndpoint(nodeInfo.Config.ApplicationName + "/" + distanceControlConfig.DistanceElementName)
	distanceController = app_distancecontrol.NewDistanceControl(uint32(distanceControlConfig.LimitValue), contactor_publisher.Message, distances_subscriber.Message)
}

// function DistanceControl_Info_Get: returns object info
func DistanceControl_Info_Get(w http.ResponseWriter, r *http.Request) {

	// prepare the json to be sent with the info of the object
	binaryJson, err := json.Marshal(DistanceControlInfo{ApiVersion: API_VERSION, Config: distanceControlConfig})

	// not possible to marshall the object send an internal error
	if err != nil {
		webhelper.JsonResponse(w, http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	// if everthing goes well send response with the json read and status OK
	webhelper.JsonResponse(w, http.StatusOK, binaryJson)
}

// function DistanceControl_Config_Get: returns GET response with DistanceControl Config
func DistanceControl_Config_Get(w http.ResponseWriter, r *http.Request) {

	// prepare the current config to be sent
	binaryJson, err := json.Marshal(distanceControlConfig)

	// if not possible to read, reponse with internal error
	if err != nil {
		webhelper.JsonResponse(w, http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	// update the response with status OK
	webhelper.JsonResponse(w, http.StatusOK, binaryJson)
}

// function DistanceControl_Config_Set: manage the patch request which write the new config
func DistanceControl_Config_Set(w http.ResponseWriter, r *http.Request) {

	// read tha payload received in request
	payload, _ := io.ReadAll(r.Body)

	// copy to a temp the actual distanceControlConfig
	var distanceControlConfig_tmp DistanceControlConfig = distanceControlConfig

	// unmarshall the payload into the temp config and return error if it doesn't work
	err := json.Unmarshal(payload, &distanceControlConfig_tmp)
	if err != nil {
		webhelper.JsonResponse(w, http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}
	// when everything goes well set the config with the new received
	distanceControlConfig = distanceControlConfig_tmp

	err = distanceControlConfig.saveToFile()
	if err != nil {
		webhelper.JsonResponse(w, http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	distanceController.Cancel()
	distances_subscriber.Cancel()
	contactor_publisher.Cancel()

	var nodeInfo node.NodeInfo
	data := webhelper.InternalGet(node.NodeInfoGet)
	err = json.Unmarshal(data, &nodeInfo)
	if err != nil {
		log.Println("Failed to fetch NodeInfo: ", err.Error())
	}

	contactor_publisher = mqttclient.NewPublisherEndpoint(nodeInfo.Config.ApplicationName + "/" + distanceControlConfig.ContactorElementName)
	distances_subscriber = mqttclient.NewSubscriberEndpoint(nodeInfo.Config.ApplicationName + "/" + distanceControlConfig.DistanceElementName)
	distanceController = app_distancecontrol.NewDistanceControl(uint32(distanceControlConfig.LimitValue), contactor_publisher.Message, distances_subscriber.Message)

	// everything ok, update the status to NO CONTENT
	w.WriteHeader(http.StatusNoContent)
}

// func loadFromFile: read the config file from filesystem and load into the object
func (obj *DistanceControlConfig) loadFromFile() error {
	var err error

	newObj := *obj
	err = filestorage.LoadObject(configurationFilePath, &newObj)
	// if it got no errors set the config with the new read
	if err == nil {
		*obj = newObj
	}

	return err
}

// func saveToFile: read the object and save to the config file
func (obj *DistanceControlConfig) saveToFile() error {
	err := filestorage.StoreObject(configurationFilePath, obj)

	return err
}
