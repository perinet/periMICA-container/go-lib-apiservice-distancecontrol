/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package distancecontrol

import (
	"encoding/json"
	"reflect"
	"testing"
	"time"

	helper "gitlab.com/perinet/generic/lib/utils/webhelper"
	"gotest.tools/v3/assert"
)

// Test Get/Set of the ContactorElementName
func TestInfo(t *testing.T) {
	var err error

	data := helper.InternalGet(DistanceControl_Info_Get)
	var distanceControlInfo DistanceControlInfo
	err = json.Unmarshal(data, &distanceControlInfo)
	assert.NilError(t, err)
	assert.Assert(t, distanceControlInfo.ApiVersion == API_VERSION)
}

// Test Get/Set of the ContactorElementName
func TestConfigContactorElementName(t *testing.T) {
	var err error
	var data []byte

	data = helper.InternalGet(DistanceControl_Config_Get)
	var distanceControlConfig DistanceControlConfig
	err = json.Unmarshal(data, &distanceControlConfig)
	assert.NilError(t, err)

	distanceControlConfig.ContactorElementName = "contactor element name"
	data, err = json.Marshal(distanceControlConfig)
	assert.NilError(t, err)
	helper.InternalPatch(DistanceControl_Config_Set, data)

	data = helper.InternalGet(DistanceControl_Config_Get)

	var distConfig DistanceControlConfig
	err = json.Unmarshal(data, &distConfig)
	assert.NilError(t, err)
	assert.Assert(t, distConfig == distanceControlConfig)
}

// Test Get/Set of the DistanceElementName
func TestConfigDistanceElementName(t *testing.T) {
	var err error
	var data []byte

	data = helper.InternalGet(DistanceControl_Config_Get)
	var distanceControlConfig DistanceControlConfig
	err = json.Unmarshal(data, &distanceControlConfig)
	assert.NilError(t, err)

	distanceControlConfig.DistanceElementName = "distance element name"
	data, err = json.Marshal(distanceControlConfig)
	assert.NilError(t, err)
	helper.InternalPatch(DistanceControl_Config_Set, data)

	data = helper.InternalGet(DistanceControl_Config_Get)

	var distConfig DistanceControlConfig
	err = json.Unmarshal(data, &distConfig)
	assert.NilError(t, err)
	assert.Assert(t, distConfig == distanceControlConfig)
}

// Test Get/Set of the LimitValue
func TestConfigLimitValue(t *testing.T) {
	var err error
	var data []byte

	data = helper.InternalGet(DistanceControl_Config_Get)
	var distanceControlConfig DistanceControlConfig
	err = json.Unmarshal(data, &distanceControlConfig)
	assert.NilError(t, err)

	distanceControlConfig.LimitValue = time.Now().Minute()
	data, err = json.Marshal(distanceControlConfig)
	assert.NilError(t, err)
	helper.InternalPatch(DistanceControl_Config_Set, data)

	data = helper.InternalGet(DistanceControl_Config_Get)

	var distConfig DistanceControlConfig
	err = json.Unmarshal(data, &distConfig)
	assert.NilError(t, err)
	assert.Assert(t, distConfig == distanceControlConfig)
}

// test function which loads the config file from the filesystem
func TestLoadFile(t *testing.T) {
	var err error
	var data []byte

	configurationFilePath = t.TempDir() + configurationFilePath
	err = distanceControlConfig.loadFromFile()
	assert.Assert(t, err != nil)

	var distanceControlConfig_test DistanceControlConfig = DistanceControlConfig{
		ContactorElementName: "contactor_switch",
		DistanceElementName:  "distance",
		LimitValue:           100,
	}

	data, err = json.Marshal(distanceControlConfig_test)
	assert.NilError(t, err)
	helper.InternalPatch(DistanceControl_Config_Set, data)

	var distanceControlConfig_empty DistanceControlConfig

	err = distanceControlConfig_empty.loadFromFile()
	assert.Assert(t, err == nil)
	assert.Assert(t, reflect.DeepEqual(distanceControlConfig_empty, distanceControlConfig_test) == true)
}
